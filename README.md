# [scratch-cacerts](https://git.dotya.ml/wanderer-containers/scratch-cacerts)

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/wanderer-containers/scratch-cacerts/status.svg?ref=refs/heads/development)](https://drone.dotya.ml/wanderer-containers/scratch-cacerts)
[![Docker Image Version](https://img.shields.io/docker/v/immawanderer/scratch-cacerts/linux-amd64)](https://hub.docker.com/r/immawanderer/scratch-cacerts/tags/?page=1&ordering=last_updated&name=linux-amd64)
[![Docker Image Size (tag)](https://img.shields.io/docker/image-size/immawanderer/scratch-cacerts/linux-amd64)](https://hub.docker.com/r/immawanderer/scratch-cacerts/tags/?page=1&ordering=last_updated&name=linux-amd64)
[![Docker pulls](https://img.shields.io/docker/pulls/immawanderer/scratch-cacerts)](https://hub.docker.com/r/immawanderer/scratch-cacerts/)

This repository provides a Containerfile to get a container image containing the most recent CA certificate bundle shipped with Alpine.

The image is rebuilt weekly in CI and automatically pushed to [DockerHub](https://hub.docker.com/r/immawanderer/scratch-cacerts).

The project lives at [this Gitea instance](https://git.dotya.ml/wanderer-containers/scratch-cacerts).

### What you get
* latest CA certificate bundle

### Purpose
* base image for apps that can live in a scratch container and only need the CA
  certificate bundle.

### License
GPL-3.0-or-later (see [LICENSE](LICENSE) for details).
